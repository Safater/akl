package akl.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;

public class Util {
    private Util() {
    }

    public static void WaitStreamAvailability(InputStream stream, int timeoutInSecs) throws IOException, InterruptedException {
        double duration = 0;
        while (stream.available() == 0 && duration < timeoutInSecs) {
            duration += 0.01;
            Thread.sleep(10);
        }
    }

    public static byte[] IntToB(int i) {
        return new byte[]{
                (byte) (i >>> 24),
                (byte) (i >>> 16),
                (byte) (i >>> 8),
                (byte) (i)
        };
    }

    public static int ByteToI(byte[] b) {
        return   b[3] & 0xFF |
                (b[2] & 0xFF) << 8 |
                (b[1] & 0xFF) << 16 |
                (b[0] & 0xFF) << 24;
    }
}
