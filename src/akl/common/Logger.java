package akl.common;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;

public class Logger {
    private static final Object LoggerSync = new Object();
    private static final StringBuilder content = new StringBuilder();

    private Logger() {
    }

    public static void Log(Level level, String message) {
        Log(level, null, message);
    }

    // todo this is slow, can improve if required
    public static void Log(Level level, String node, String message) {
        synchronized (LoggerSync) {
            File logFile = new File(Settings.Instance.LogFile);
            if (! logFile.exists()) {
                try {
                    logFile.createNewFile();
                } catch (IOException ignored) { }
            }
            String newLog = "";
            FileWriter writer = null;
            try {
                writer = new FileWriter(Settings.Instance.LogFile, true);
                switch (level) {
                    case Info:
                        newLog = createMessage("INFO", node, message);
                        break;
                    case Error:
                        newLog = createMessage("ERROR", node, message);
                        break;
                    case Fatal:
                        newLog = createMessage("FATAL", node, message);
                        break;
                }
                writer.write(newLog + System.lineSeparator());
                writer.flush();
            } catch (Exception ignored) {
            } finally {
                try {
                    if (writer != null) {
                        writer.close();
                    }
                } catch (Exception ignored) {}
            }
            content.append(newLog).append("\r\n");
        }
    }
    
    private static String createMessage(String level, String node, String message) {
        StringBuilder log = new StringBuilder();

        Date currentTime = Date.from(Instant.now());
        log.append("[").append(level).append("] ");
        if (node != null) {
            log.append("[").append(node).append("] ");
        }
        log.append("[").append(currentTime.getTime()).append("]: ");
        log.append(message);

        return log.toString();
    }

    public static String Content() {
        return content.toString();
    }

    public static String BuildMessage(String s, String ... args) {
        String strToBuild = s;

        int index = 0;
        while (s.contains("%" + index) && index < args.length) {
            strToBuild = strToBuild.replace("%" + index, "[" + args[index] + "]");
            index++;
        }

        return strToBuild;
    }

    public enum Level {
        Info,
        Error,
        Fatal
    }
}
