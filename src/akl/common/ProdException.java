package akl.common;

public class ProdException extends Exception {
    public static final ProdException RUN_BEFORE_STOP = new ProdException("You need to run the service before trying to stop it.");
    public static final ProdException SS_NOT_NULL = new ProdException("The server socket cannot be null when initializing RSListenInstance.");
    public static final ProdException UNABLE_TO_SERIALIZE = new ProdException("Unable to serialize message.");
    public static final ProdException UNABLE_TO_DESERIALIZE = new ProdException("Unable to deserialize message.");
    public static final ProdException UNSET_CALLBACK = new ProdException("You need to properly set callback before sending data.");
    public static final ProdException PATH_MUST_BE_DIR = new ProdException("The path specified must be a directory.");
    public static final ProdException NODE_NOT_FOUND = new ProdException("The path is not found in one of the clusters configurations.");
    public static final ProdException INVALID_JSON = new ProdException("The specified JSON is invalid.");
    public static final ProdException CLUSTER_NOT_LAUNCHED = new ProdException("You must launch the cluster first to shut it down.");
    public static final ProdException CLUSTER_ALREADY_RUNNING = new ProdException("You cannot run two clusters at the same time.");
    public static final ProdException NOT_SUPPORTED = new ProdException("This function is not supported in this class.");
    public static final ProdException CANNOT_CREATE_SAVE_FILE = new ProdException("Cannot create the save file.");
    public static final ProdException CANNOT_LOAD_SETTINGS_FILE = new ProdException("Cannot load the save file.");
    public static final ProdException CANNOT_SAVE_SETTINGS_FILE = new ProdException("Cannot save the save file.");
    public static final ProdException NEED_TO_LAUNCH_BEFORE_SPAWN = new ProdException("You need to launch the cluster before you can spawn a client.");
    public static final ProdException NODE_NOT_SELECTED = new ProdException("You must select a node to read/write.");
    public static final ProdException ACCESS_BLOCKED_TO_RESOURCE = new ProdException("The requested resource is currently locked.");

    private ProdException(String message) {
        super(message);
    }

    public static void ThrowAsAKLException(Exception ex) throws ProdException {
        throw new ProdException(ex.getMessage());
    }

    public static ProdException INVALID_RECIPIENT_P1(String p1) {
        if (p1 == null) {
            p1 = "null";
        }

        return new ProdException(String.format("The recipient [%s] is invalid", p1));
    }

    public static ProdException NODE_WITH_P1_NOT_EXISTS(String p1) {
        if (p1 == null) {
            p1 = "null";
        }

        return new ProdException(String.format("The node with id [%s] does not exist.", p1));
    }

    public static ProdException P1_CANT_BE_NULL(String p1) {
        return new ProdException(String.format("%s cannot be null.", p1));
    }

    public static ProdException INCORRECT_FORMAT_FOR_P1(String p1) {
        return new ProdException(String.format("The format specified for %s is incorrect.", p1));
    }

    public static ProdException PATH_P1_NOT_FOUND(String p1) {
        return new ProdException(String.format("The path specified [%s] is not found.", p1));
    }

    public static ProdException UNABLE_TO_CREATE_LOCK_P1(String p1) {
        return new ProdException(String.format("Unable to create lock: [%s].", p1));
    }
}
