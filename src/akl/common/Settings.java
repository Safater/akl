package akl.common;

import org.omg.PortableInterceptor.INACTIVE;

import java.io.*;

public class Settings {
    private static final Object InstanceLock = new Object();
    private static final String FNAME = "Common.txt";

    public static Settings Instance = new Settings();


    private String saveFile;

    public String LogFile = "null";
    public String InitialClusterLocation = "null";

    private Settings()  {
        try {
            load();
        } catch (Exception ignored) {}
    }

    private void ensureLocation() throws ProdException {
        String appdata = System.getenv("APPDATA");
        String joinedDir = new File(appdata, "DistributedLocking").toString();

        if (! new File(joinedDir).exists()) {
            new File(joinedDir).mkdir();
        }

        saveFile = new File(joinedDir, FNAME).toString();

        if (! new File(saveFile).exists()) {
            try {
                new File(saveFile).createNewFile();
            } catch (IOException e) {
                throw ProdException.CANNOT_CREATE_SAVE_FILE;
            }
        }
    }
    public void load() throws ProdException {
        synchronized (InstanceLock) {
            ensureLocation();
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new FileReader(saveFile));

                String setting;
                while ((setting = bufferedReader.readLine()) != null) {
                    String[] settingSpl = setting.split("=");

                    if (settingSpl[0].equals("logfile")) {
                        LogFile = settingSpl[1];
                    }
                    if (settingSpl[0].equals("initialclusterlocation")) {
                        InitialClusterLocation = settingSpl[1];
                    }
                }
            if (LogFile.equals("null")) {
                LogFile = new File(new File(System.getenv("APPDATA"), "DistributedLocking").toString(), "log.txt").toString();
            }
            } catch (Exception ex) {
                throw ProdException.CANNOT_LOAD_SETTINGS_FILE;
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }
    }

    public void save() throws ProdException {
        synchronized (InstanceLock) {
            ensureLocation();
            BufferedWriter bufferedWriter = null;
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(saveFile));

                bufferedWriter.write("logfile=" + LogFile);
                bufferedWriter.newLine();
                bufferedWriter.write("initialclusterlocation=" + InitialClusterLocation);
            } catch (Exception ex) {
                throw ProdException.CANNOT_SAVE_SETTINGS_FILE;
            } finally {
                if (bufferedWriter != null) {
                    try {
                        bufferedWriter.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }
    }
}
