package akl.backend;

import akl.common.Logger;
import akl.common.ProdException;
import akl.common.Util;

import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class Client implements IChannel, AutoCloseable {
    private Socket client = null;
    private ICallback callback = null;
    private int timeout = 60;
    private String guid;

    public Client(String guid) {
        client = new Socket();
        this.guid = guid;
    }

    public void connect(String ip, int port) throws ProdException {
        try {
            SocketAddress socketAddress = new InetSocketAddress(Inet4Address.getByName(ip), port);
            client.setKeepAlive(true);
            client.setReuseAddress(true);
            client.connect(socketAddress, 60);
            Logger.Log(Logger.Level.Info, Logger.BuildMessage("Client with guid %0 successfully connected to port %1", guid, port + ""));
        } catch (Exception ex) {
            ProdException.ThrowAsAKLException(ex);
        }
    }

    public void send(Message message) throws ProdException {
        send(message, false);
    }

    public void send(Message message, boolean forwardedMessage) throws ProdException {
        if (callback == null) {
            throw ProdException.UNSET_CALLBACK;
        }

        try {
            byte[] content = message.serialize();

            client.getOutputStream().write(Util.IntToB(content.length));
            client.getOutputStream().write(content);
            client.getOutputStream().flush();

            Util.WaitStreamAvailability(client.getInputStream(), timeout);

            if (! forwardedMessage) {
                callback.callback(Message.Load(client.getInputStream()));
            }
        } catch (Exception ex) {
            ProdException.ThrowAsAKLException(ex);
        }
    }

    public void disconnect() throws ProdException {
        if (client != null && client.isConnected()) {
            try {
                client.close();
            } catch (Exception ex) {
                ProdException.ThrowAsAKLException(ex);
            }
        }
    }

    public void receive(Message message) throws ProdException {
        throw ProdException.NOT_SUPPORTED;
    }

    public void setCallback(ICallback callback) {
        this.callback = callback;
    }

    public void close() throws ProdException {
        disconnect();
    }
}
