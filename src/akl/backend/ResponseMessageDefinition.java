package akl.backend;

import akl.common.ProdException;
import org.json.JSONException;
import org.json.JSONObject;

public class ResponseMessageDefinition implements IJsonResponse {
    public String FilePath;
    public String Content;
    public double ResponseTime;

    private static final String JSONFormat = "" +
            "{" +
            "   \"root\": {" +
            "       \"filepath\": \"{filepath}\"," +
            "       \"content\": \"{content}\"," +
            "       \"responsetime\": {responsetime}" +
            "   }" +
            "}";

    public void loadFromJSON(String json) throws ProdException {
        try {
            JSONObject root = new JSONObject(json);
            root = root.getJSONObject("root");
            this.FilePath = (String) root.get("filepath");
            this.Content = (String) root.get("content");
            this.ResponseTime = (Double) root.get("responsetime");
        } catch (JSONException ex) {
            throw ProdException.INVALID_JSON;
        }
    }

    public String buildJSON() {
        String json = JSONFormat;

        json = json.replace("{filepath}", FilePath);
        json = json.replace("{content}", Content);
        json = json.replace("{responsetime}", ResponseTime + "");

        return json;
    }
}