package akl.backend;

import akl.common.ProdException;

public interface IChannel {
    public void send(Message message) throws ProdException;
    public void receive(Message message) throws ProdException;
}
