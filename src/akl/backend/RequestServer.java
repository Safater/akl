package akl.backend;

import akl.common.ProdException;

import java.io.IOException;
import java.net.ServerSocket;

public class RequestServer {
    private ServerSocket serverSocket;
    private Thread runningService = null;
    private RSListenInstance listener = null;
    private ICallback callback;

    public RequestServer(ICallback callback, int port) throws  ProdException {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException ex) {
            ProdException.ThrowAsAKLException(ex);
        }

        listener = new RSListenInstance(callback, serverSocket);
        this.callback = callback;
    }

    public void start() throws ProdException {
        runningService = new Thread(listener);
        runningService.start();

        if (listener.PendingException != null) {
            try {
                throw listener.PendingException;
            } catch (Exception ex) {
                ProdException.ThrowAsAKLException(ex);
            }
        }
    }

    public void stop() throws ProdException {
        if (runningService == null) {
            throw ProdException.RUN_BEFORE_STOP;
        }

        try {
            serverSocket.close();
        } catch (IOException ex) {
            ProdException.ThrowAsAKLException(ex);
        }

        runningService.interrupt();

        while (! listener.Launched) {}

        RSListenInstance.stop(listener.getUid());
    }
}
