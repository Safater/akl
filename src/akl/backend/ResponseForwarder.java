package akl.backend;

import akl.common.Logger;
import akl.common.ProdException;
import akl.common.Util;

import java.io.IOException;
import java.net.Socket;

public class ResponseForwarder implements Runnable {
    private Socket client = null;
    public Exception PendingException = null;
    private ICallback callback;

    public ResponseForwarder(ICallback callback, Socket client) {
        this.client = client;
        this.callback = callback;
    }

    private void runInner() throws IOException, ProdException, InterruptedException {
        while (client.isConnected()) {
            Util.WaitStreamAvailability(client.getInputStream(), 10);
            Message message = Message.Load(client.getInputStream());
            Logger.Log(Logger.Level.Info, Logger.BuildMessage("Received message from client with socket id: %0, message GUID: %1.", message.SocketIdentifier, message.GUID));

            if (message.Other.equals("pass")) {
                client.getOutputStream().write(new byte[] { 1 });
                client.getOutputStream().flush();
            }

            this.callback.respondTo(message.SocketIdentifier, client);
            this.callback.callback(message);
        }
    }

    @Override
    public void run() {
        try {
            runInner();
        } catch (Exception ex) {
            PendingException = ex;
        }
    }
}
