package akl.backend;

import akl.common.Logger;
import akl.common.ProdException;
import akl.common.Util;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.Hashtable;

public class AKLNode implements IChannel, ICallback {
    public String Id;
    public int Port;
    public ServerStatus Status;

    private AKLLockManager lockManager;
    private RequestServer requestServer;

    private static final Hashtable<String, Socket> clients = new Hashtable<>();

    public AKLNode(String id, int port, Hashtable<String, AKLNode> serverNodeTable) throws ProdException {
        Status = ServerStatus.Idle;
        Id = id;
        Port = port;
        requestServer = new RequestServer(this, port);
        lockManager = new AKLLockManager(this, Cluster.GetLaunchDir(), serverNodeTable);
    }

    public void start() throws ProdException {
        Status = ServerStatus.Running;
        requestServer.start();
    }

    public void stop() throws ProdException {
        Status = ServerStatus.Suspended;
        requestServer.stop();
    }

    @Override
    public void send(Message message) throws ProdException {
        if (message.To == null || lockManager.getServerNode(message.To) == null) {
            throw ProdException.INVALID_RECIPIENT_P1(message.To);
        }

        if (message.To.equals(Id)) {
            receive(message);
        } else {
            lockManager.getServerNode(message.To).receive(message);
        }
    }

    @Override
    public void receive(Message message) throws ProdException {
        MessageActionDefinition actionDefinition = new MessageActionDefinition();
        actionDefinition.loadFromJSON(message.JSON);

        switch (actionDefinition.Action) {
            case ReleaseLock:
                lockManager.releaseLock(message.SocketIdentifier, actionDefinition.Subject);
                RespondWithAccess(message, Message.SUCCESS_CODE, "Lock Released");
                break;
            case RequestAccess:
                lockManager.requestAccess(message.SocketIdentifier, actionDefinition.From, actionDefinition.Subject, actionDefinition.LockMode, message.Other.equals("fwd"));
                break;
            case GrantAccess:
                RespondWithAccess(message, Message.SUCCESS_CODE, "Access Granted");
                break;
            case BlockAccess:
                RespondWithAccess(message, Message.FAIL_CODE, "Access Denied");
                break;
        }
    }

    private void RespondWithAccess(Message message, int code, String contentStr) throws ProdException {
        try {
            String socketIdentifier = message.SocketIdentifier;

            MessageActionDefinition actionDefinition = new MessageActionDefinition();
            actionDefinition.loadFromJSON(message.JSON);

            ResponseMessageDefinition response = new ResponseMessageDefinition();
            response.FilePath = new File(Cluster.GetLaunchDir(), actionDefinition.Subject).toString().replace("\\", "\\\\");
            response.ResponseTime = 1.0;
            response.Content = contentStr;

            Message responseMessage = new Message();
            responseMessage.Code = code;
            responseMessage.JSON = response.buildJSON();
            responseMessage.SocketIdentifier = socketIdentifier;

            sendMessageToClient(responseMessage, socketIdentifier);
        } catch (Exception ex) {
            ProdException.ThrowAsAKLException(ex);
        }
    }

    private void sendMessageToClient(Message message, String socketIdentifier) throws IOException, ProdException {
        Socket sock = clients.get(socketIdentifier);

        byte[] content = message.serialize();
        sock.getOutputStream().write(Util.IntToB(content.length));
        sock.getOutputStream().write(content, 0, content.length);
        sock.getOutputStream().flush();
    }

    @Override
    public void respondTo(String socketIdentifier, Socket client) {
        synchronized (clients) {
            clients.put(socketIdentifier, client);
        }
    }

    @Override
    public void callback(Message message) throws ProdException {
        if (! message.Other.equals("pass")) {
            MessageActionDefinition mad = new MessageActionDefinition();
            mad.loadFromJSON(message.JSON);
        }

        this.receive(message);
    }

    public enum ServerStatus {
        Running,
        Suspended,
        Idle
    }
}
