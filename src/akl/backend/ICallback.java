package akl.backend;

import akl.common.ProdException;

import java.net.Socket;

public interface ICallback {
    public void respondTo(String messageGuid, Socket client);
    public void callback(Message message) throws ProdException;
}
