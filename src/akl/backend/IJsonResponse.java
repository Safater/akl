package akl.backend;

import akl.common.ProdException;

public interface IJsonResponse {
    public void loadFromJSON(String json) throws ProdException;
    public String buildJSON();
}
