package akl.backend;

import akl.common.Logger;
import akl.common.ProdException;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Hashtable;

public class RSListenInstance implements Runnable {
    public Exception PendingException = null;
    public boolean Launched = false;

    private ServerSocket serverSocket = null;
    private boolean running = true;

    private static Hashtable<Integer, Boolean> StopTrigger = new Hashtable<>();
    private static Hashtable<Integer, ArrayList<Thread>> ResponseThreads = new Hashtable<>();
    private static Hashtable<Integer, ArrayList<Socket>> ConnectedClients = new Hashtable<>();

    private static int globUuid = 0;
    private int uid = 0;
    private static final Object sync = new Object();
    private ICallback callback;

    private void init() {
        synchronized (sync) {
            ++globUuid;
            this.uid = globUuid;
            ResponseThreads.put(uid, new ArrayList<>());
            ConnectedClients.put(uid, new ArrayList<>());
        }
    }

    public RSListenInstance(ICallback callback, ServerSocket serverSocket) throws ProdException {
        if (serverSocket == null) {
            throw ProdException.SS_NOT_NULL;
        }

        init();

        this.serverSocket = serverSocket;
        StopTrigger.put(uid, false);
        this.callback = callback;
    }

    private void runInner() throws Exception {
        while (! StopTrigger.get(uid)) {
            Socket client = serverSocket.accept();
            client.setKeepAlive(true);
            Logger.Log(Logger.Level.Info, Logger.BuildMessage("Listen service with uuid %0 accepted a client.", uid + ""));
            ResponseForwarder clientHandler = new ResponseForwarder(this.callback, client);
            Thread responseHandler = new Thread(clientHandler);
            responseHandler.start();
            ResponseThreads.get(uid).add(responseHandler);
            ConnectedClients.get(uid).add(client);
            if (clientHandler.PendingException != null) {
                throw clientHandler.PendingException;
            }
        }
    }

    public static void stop(int uid) throws ProdException {
        synchronized (sync) {
            StopTrigger.put(uid, true);

            if (ResponseThreads.containsKey(uid)) {
                for (Thread rt : ResponseThreads.get(uid)) {
                    rt.interrupt();
                }
            }

            if (ConnectedClients.containsKey(uid)) {
                for (Socket s : ConnectedClients.get(uid)) {
                    try {
                        s.close();
                    } catch (Exception ex) {
                        ProdException.ThrowAsAKLException(ex);
                    }
                }
            }
        }
    }

    public int getUid() {
        return uid;
    }

    @Override
    public void run() {
        try {
            runInner();
        } catch (Exception pendingException) {
            PendingException = pendingException;
        } finally {
            Launched = true;
        }
    }
}
