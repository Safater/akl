package akl.backend;

import akl.common.ProdException;
import akl.common.Util;

import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;

/*
Object based streaming is too complicated for the moment. IE. when a file is asked to be read the whole file will be read at once, passing a stream is too complicated. PAGING is possible later.
 */
public class Message implements Serializable {
    public static final int SUCCESS_CODE = 200;
    public static final int FAIL_CODE = 400;


    private static final long serialVersionUID = 7526472295733886147L;

    public final String GUID = java.util.UUID.randomUUID().toString();
    public int Code = -1;
    public String To = "";
    public String JSON = "";
    public String SocketIdentifier = java.util.UUID.randomUUID().toString();
    public String Other = "";

    private static final String SEPARATOR = "%EOCOMP% 3647232213--";


    public void deserialize(byte[] b) throws ProdException {
        try {
            String message = new String(b, "utf8");
            message = URLDecoder.decode(message, "utf8");

            String[] l = message.split(SEPARATOR);
            Code = Integer.parseInt(l[0]);
            To = l[1];
            JSON = l[2];
            SocketIdentifier = l[3];
            Other = l[4];
        } catch (Exception ex) {
            throw ProdException.UNABLE_TO_DESERIALIZE;
        }
    }

    public byte[] serialize() throws ProdException {
        StringBuilder message = new StringBuilder();
        message.append(Code).append(SEPARATOR);
        message.append(To).append(SEPARATOR);
        message.append(JSON).append(SEPARATOR);
        message.append(SocketIdentifier).append(SEPARATOR);
        message.append(Other).append(SEPARATOR);

        try {
            return URLEncoder.encode(message.toString(), "utf8").getBytes("utf8");
        } catch (UnsupportedEncodingException e) {
            throw ProdException.UNABLE_TO_SERIALIZE;
        }
    }

    public static Message Load(InputStream stream) throws IOException {
        Message aklMessage = new Message();

        int loc = 0;

        byte[] contentLengthB = new byte[4];
        int bytesRead = stream.read(contentLengthB);
        int contentLength = 0;

        if (bytesRead > 0) {
            contentLength = Util.ByteToI(contentLengthB);
        }

        byte[] buffer = new byte[1024];
        StringBuilder content = new StringBuilder();

        while (contentLength > 0) {
            bytesRead = stream.read(buffer);
            if (bytesRead > 0) {
                String part = new String(buffer, 0, bytesRead, "utf8");
                content.append(URLDecoder.decode(part, "utf8"));
                contentLength -= bytesRead;
            }
        }

        for (String innerLine : content.toString().split(SEPARATOR)) {
            switch (loc) {
                case 0:
                    aklMessage.Code = Integer.parseInt(innerLine);
                    break;
                case 1:
                    aklMessage.To = innerLine;
                    break;
                case 2:
                    aklMessage.JSON = innerLine;
                    break;
                case 3:
                    aklMessage.SocketIdentifier = innerLine;
                    break;
                case 4:
                    aklMessage.Other = innerLine;
                    break;
            }
            ++loc;
        }

        return aklMessage;
    }

}
