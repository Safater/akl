package akl.backend;

import akl.common.ProdException;
import org.json.*;

public class MessageActionDefinition implements IJsonResponse {
    private static final String REQUESTACCESS = "requestaccess";
    private static final String GRANTACCESS = "grantaccess";
    private static final String BLOCKACCESS = "blockaccess";
    private static final String RELEASELOCK = "releaselock";

    public EAction Action;
    public String From = "";
    public String Subject = "";
    public AKLLockManager.LockMode LockMode = AKLLockManager.LockMode.Read;

    private static final String JSONFormat = "" +
            "{" +
            "   \"root\": {" +
            "       \"from\": \"{from}\"," +
            "       \"permission\": {permission}," +
            "       \"action\": {" +
            "           \"name\": \"{actionname}\"," +
            "           \"subject\": \"{subject}\"" +
            "       }" +
            "   }" +
            "}";

    public void loadFromJSON(String json) throws ProdException {
        try {
            JSONObject root = new JSONObject(json);
            root = root.getJSONObject("root");
            this.From = (String) root.get("from");
            JSONObject action = root.getJSONObject("action");

            String actionName = (String) action.get("name");
            this.Subject = (String) action.get("subject");

            if (actionName.equals(REQUESTACCESS)) {
                this.Action = EAction.RequestAccess;
            }

            if (actionName.equals(GRANTACCESS)) {
                this.Action = EAction.GrantAccess;
            }

            if (actionName.equals(BLOCKACCESS)) {
                this.Action = EAction.BlockAccess;
            }

            if (actionName.equals(RELEASELOCK)) {
                this.Action = EAction.ReleaseLock;
            }

            int permission = (Integer) root.get("permission");

            if (permission == 0) {
                this.LockMode = AKLLockManager.LockMode.None;
            } else if (permission == 1) {
                this.LockMode = AKLLockManager.LockMode.Read;
            } else {
                this.LockMode = AKLLockManager.LockMode.Write;
            }
        } catch (JSONException ex) {
            throw ProdException.INVALID_JSON;
        }
    }

    public String buildJSON() {
        String json = JSONFormat;
        json = json.replace("{from}", From);
        json = json.replace("{subject}", Subject.replace("\\", "\\\\"));

        switch (this.Action) {
            case RequestAccess:
                json = json.replace("{actionname}", REQUESTACCESS);
                break;
            case GrantAccess:
                json = json.replace("{actionname}", GRANTACCESS);
                break;
            case BlockAccess:
                json = json.replace("{actionname}", BLOCKACCESS);
                break;
            case ReleaseLock:
                json = json.replace("{actionname}", RELEASELOCK);
                break;
        }

        switch (this.LockMode) {
            case Write:
                json = json.replace("{permission}", 2 + "");
                break;
            case Read:
                json = json.replace("{permission}", 1 + "");
                break;
            case None:
                json = json.replace("{permission}", 0 + "");
                break;
        }
        return json;
    }

    public enum EAction {
        RequestAccess,
        GrantAccess,
        BlockAccess,
        ReleaseLock
    }
}
