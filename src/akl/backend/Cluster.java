package akl.backend;

import akl.common.Logger;
import akl.common.ProdException;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

public class Cluster {
    private static final Hashtable<String, AKLNode> serverNodeTable = new Hashtable<>();

    private static final Object LaunchSync = new Object();
    private static int portCounter = 9999;
    private static String launchDir = "";
    private static boolean running = false;

    public static void LaunchCluster(String directory) throws ProdException {
        synchronized (LaunchSync) {
            Logger.Log(Logger.Level.Info, Logger.BuildMessage("Launching cluster at directory: %0.", directory));
            if (running) {
                throw ProdException.CLUSTER_ALREADY_RUNNING;
            }

            running = true;
            launchDir = directory;

            if (directory == null || new File(directory).isFile()) {
                throw ProdException.PATH_MUST_BE_DIR;
            }

            if (!directory.endsWith(File.separator)) {
                directory += File.separator;
            }

            File dir = new File(directory);

            for (String node : dir.list()) {
                CreateNode(directory + node);
            }
        }
    }

    private static void CreateNode(String node) throws  ProdException {
        File f = new File(node);

        if (f.isDirectory()) {
            String id = f.getName();
            int nodePort = portCounter++;
            serverNodeTable.put(id.toLowerCase(), new AKLNode(id.toLowerCase(), nodePort, serverNodeTable));
            serverNodeTable.get(id.toLowerCase()).start();
            Logger.Log(Logger.Level.Info, Logger.BuildMessage("Creating node with Id: %0 at port: %1.", id.toLowerCase(), nodePort + ""));
        }
    }

    public static boolean NodeExists(String id) throws ProdException {
        synchronized (LaunchSync) {
            return serverNodeTable.containsKey(id.toLowerCase());
        }
    }

    public static ArrayList<AKLNode> GetNodeList() {
        synchronized (LaunchSync) {
            ArrayList<AKLNode> nodes = new ArrayList<>();

            Enumeration enumeration = serverNodeTable.keys();
            while (enumeration.hasMoreElements()) {
                nodes.add(serverNodeTable.get(enumeration.nextElement()));
            }

            return nodes;
        }
    }

    public static AKLNode GetNode(String id) throws ProdException {
        synchronized (LaunchSync) {
            if (serverNodeTable.containsKey(id.toLowerCase())) {
                return serverNodeTable.get(id.toLowerCase());
            }

            throw ProdException.NODE_NOT_FOUND;
        }
    }

    public static void Shutdown() throws ProdException {
        synchronized (LaunchSync) {
            if (! running) {
                return;
            }
            Enumeration en = serverNodeTable.keys();

            while (en.hasMoreElements()) {
                String nodeId = (String) en.nextElement();
                AKLNode node = serverNodeTable.get(nodeId);
                node.stop();
            }

            running = false;
        }
    }

    public static String GetLaunchDir() {
        synchronized (LaunchSync) {
            return launchDir;
        }
    }
}
