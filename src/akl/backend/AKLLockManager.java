package akl.backend;

import akl.common.Logger;
import akl.common.ProdException;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class AKLLockManager {
    private Hashtable<String, RequestLock> RequestingLockTable = new Hashtable<>();
    private Hashtable<String, AKLNode> ServerNodeTable = new Hashtable<>();
    private Hashtable<String, LFLEntity> LockedFileList = new Hashtable<>();

    private String fileDirectory;

    private AKLNode OwnerNode;

    public AKLLockManager(AKLNode ownerNode, String fileDirectory, Hashtable<String, AKLNode> serverNodeTable) throws ProdException {
        this.fileDirectory = fileDirectory;

        if (new File(this.fileDirectory).isFile()) {
            throw ProdException.PATH_MUST_BE_DIR;
        }

        if (! this.fileDirectory.endsWith(File.separator)) {
            this.fileDirectory += File.separator;
        }

        this.ServerNodeTable = serverNodeTable;
        this.OwnerNode = ownerNode;
    }

    synchronized public AKLNode getServerNode(String id) {
        if (ServerNodeTable.containsKey(id.toLowerCase())) {
            return ServerNodeTable.get(id.toLowerCase());
        }

        return null;
    }

    synchronized public void requestAccess(String socketIdentifier, String from, String path, LockMode mode, boolean forwarded) throws ProdException {
        if (path == null) {
            throw ProdException.P1_CANT_BE_NULL("path");
        }

        String requestFile = this.fileDirectory + path;

        if (! new File(requestFile).exists()) {
            throw ProdException.PATH_P1_NOT_FOUND(path);
        }

        String lockingServerId = getLockingServerId(path);

        if (lockingServerId == null) {
            initializeOwnership(socketIdentifier, from, path, mode);
        } else {
            if (! ServerNodeTable.containsKey(lockingServerId.toLowerCase())) {
                throw ProdException.NODE_NOT_FOUND;
            }

            processAccess(socketIdentifier, from, lockingServerId, path, mode);
        }
    }

    private String getLockingServerId(String path) throws ProdException {
        String requestFile = this.fileDirectory + path;

        String lockingServerId = null;

        File fileDirectoryF = new File(requestFile).getParentFile();

        File[] files = fileDirectoryF.listFiles();

        if (files == null || files.length == 0) {
            throw ProdException.PATH_P1_NOT_FOUND(path);
        }

        String requestingFileName = new File(requestFile).getName();

        for (File f: files) {
            if (f.getName().startsWith(requestingFileName + ".lock")) {
                String lockDef = f.getName().substring(f.getName().lastIndexOf(".")).toLowerCase();
                lockingServerId = lockDef.substring(1);
            }
        }

        return lockingServerId;
    }

    synchronized public void releaseLock(String socketIdentifier, String path) throws ProdException {
        String lockingServerId = getLockingServerId(path);

        if (lockingServerId.equals(OwnerNode.Id)) {
            // Grant or block access.
            Logger.Log(Logger.Level.Info, OwnerNode.Id, Logger.BuildMessage("Releasing lock for path: %0.", path));
            LFLEntity lockedEntity = getLockedEntity(path);
            lockedEntity.Lock.LockMode = LockMode.None;
            lockedEntity.Lock.RequestPath = path;
        } else {
            // Ask the responsible node to grant or block access
            Logger.Log(Logger.Level.Info, OwnerNode.Id, Logger.BuildMessage("Lock belongs to %0 forwarding release lock request for path: %1.", lockingServerId, path));
            MessageActionDefinition requestAccessDefinition = new MessageActionDefinition();
            requestAccessDefinition.From = OwnerNode.Id;
            requestAccessDefinition.Action = MessageActionDefinition.EAction.ReleaseLock;
            requestAccessDefinition.Subject = path;
            requestAccessDefinition.LockMode = LockMode.None;

            Message releaseLockMessage = new Message();
            releaseLockMessage.To = lockingServerId;
            releaseLockMessage.JSON = requestAccessDefinition.buildJSON();
            releaseLockMessage.SocketIdentifier = socketIdentifier;
            OwnerNode.send(releaseLockMessage);
        }
    }

    private LFLEntity getLockedEntity(String path) {
        String key = path.toLowerCase();

        LFLEntity lockedEntity = LockedFileList.get(key);

        if (lockedEntity == null) {
            lockedEntity = new LFLEntity();
            lockedEntity.Lock = new RequestLock();
            lockedEntity.Lock.LockMode = LockMode.None;
            lockedEntity.Lock.RequestPath = path;

            LockedFileList.put(key, lockedEntity);
        }

        return lockedEntity;
    }

    private void processAccess(String socketIdentifier, String from, String lockingServerId, String path, LockMode lockMode) throws ProdException {
        Logger.Log(Logger.Level.Info, OwnerNode.Id, Logger.BuildMessage("Processing access for path: %0 with lock mode %1.", path, lockMode.name()));

        if (lockingServerId.equals(OwnerNode.Id)) {
            // Grant or block access.
            LFLEntity lockedEntity = getLockedEntity(path);
            MessageActionDefinition.EAction responseAccess = MessageActionDefinition.EAction.GrantAccess;

            // deny write-anything
            if (lockMode == LockMode.Write && lockedEntity.Lock.LockMode != LockMode.None || lockedEntity.Lock.LockMode == LockMode.Write) {
                responseAccess = MessageActionDefinition.EAction.BlockAccess;
            }

            RequestLock requestLock = new RequestLock();
            requestLock.LockMode = lockMode;
            requestLock.RequesterId = from;
            requestLock.RequestPath = path;
            requestLock.Timestamp = Calendar.getInstance().getTimeInMillis();

            if (responseAccess == MessageActionDefinition.EAction.GrantAccess) {
                lockedEntity.RequesterServer = from; // todo I don't know why this is necessary, statistics only?

                lockedEntity.Lock = requestLock;
                lockedEntity.GrantedLocks.add(requestLock);
                Logger.Log(Logger.Level.Info, OwnerNode.Id, Logger.BuildMessage("Granted access for path: %0 with lock mode %1.", path, lockMode.name()));
            } else {
                Logger.Log(Logger.Level.Info, OwnerNode.Id, Logger.BuildMessage("Denied access for path: %0 with lock mode %1.", path, lockMode.name()));
                lockedEntity.BlockedLocks.add(requestLock);
            }

            MessageActionDefinition actionDefinition = new MessageActionDefinition();
            actionDefinition.From = from;
            actionDefinition.Action = responseAccess;
            actionDefinition.Subject = path;
            actionDefinition.LockMode = lockMode;

            Message message = new Message();
            message.To = ServerNodeTable.get(lockingServerId).Id;
            message.JSON = actionDefinition.buildJSON();
            message.SocketIdentifier = socketIdentifier;

            OwnerNode.send(message);
        } else {
            // Ask the responsible node to grant or block access
            Logger.Log(Logger.Level.Info, OwnerNode.Id, Logger.BuildMessage("Lock belongs to %0 forwarding process access request for path: %1.", lockingServerId, path));
            MessageActionDefinition requestAccessDefinition = new MessageActionDefinition();
            requestAccessDefinition.From = OwnerNode.Id;
            requestAccessDefinition.Action = MessageActionDefinition.EAction.RequestAccess;
            requestAccessDefinition.Subject = path;
            requestAccessDefinition.LockMode = lockMode;

            Message requestAccessMessage = new Message();
            requestAccessMessage.To = lockingServerId;
            requestAccessMessage.JSON = requestAccessDefinition.buildJSON();
            requestAccessMessage.SocketIdentifier = socketIdentifier;
            OwnerNode.send(requestAccessMessage);
        }
    }

    private void initializeOwnership(String socketIdentifier, String from, String requestFileLock, LockMode lockMode) throws ProdException {
        String lockingServerId = OwnerNode.Id;

        // request is supposed to also have the node id by convention ie. node1\request
        if (requestFileLock.contains(File.separator)) {
            lockingServerId = requestFileLock.substring(0, requestFileLock.indexOf(File.separator));
        }

        String toCreatePath = fileDirectory + requestFileLock + ".lock." + lockingServerId;

        File toCreate = new File(toCreatePath);

        try {
            Logger.Log(Logger.Level.Info, OwnerNode.Id, Logger.BuildMessage("No node is locking %0 assuming ownership.", requestFileLock));
            toCreate.createNewFile();
        } catch (IOException e) {
            ProdException.ThrowAsAKLException(e);
        }

        processAccess(socketIdentifier, from, lockingServerId, requestFileLock, lockMode);
    }

    class LFLEntity {
        public String RequesterServer;
        public RequestLock Lock;
        public final List<RequestLock> GrantedLocks = new ArrayList<>();
        public final List<RequestLock> BlockedLocks = new ArrayList<>();
    }

    public class RequestLock {
        public String RequesterId;
        public LockMode LockMode;
        public String RequestPath;
        public long Timestamp;
    }

    public enum LockMode {
        Write,
        Read,
        None
    }
}
