package ui;

import akl.backend.AKLNode;
import akl.backend.Client;
import akl.backend.Cluster;
import akl.common.ProdException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Popup;
import javafx.stage.PopupWindow;
import javafx.stage.Stage;
import javafx.stage.Window;

public class ClientScene {
    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;

    private String uuid;
    private ClientController controller;

    public ClientScene(String uuid) {
        this.uuid = uuid;
    }

    private String edditingFile = "";

    public Stage launch() {
        controller = new ClientController(uuid);

        Stage clientWindow = new Stage();
        clientWindow.setTitle("Client: " + uuid);
        clientWindow.setResizable(false);
        clientWindow.setMinWidth(WIDTH);
        clientWindow.setMaxWidth(WIDTH);
        clientWindow.setMinHeight(HEIGHT);
        clientWindow.setMaxHeight(HEIGHT);

        Group root = new Group();
        Scene mainScene = new Scene(root);

        clientWindow.setScene(mainScene);

        TextArea content = new TextArea();
        content.setEditable(false);
        content.setWrapText(true);
        content.setMinHeight(268);
        content.setMaxWidth(WIDTH - 27);

        Label requestLabel = new Label();
        requestLabel.setText("Request path: ");
        requestLabel.setLayoutX(10);
        requestLabel.setLayoutY(22);

        Label issueRequestTo = new Label();
        issueRequestTo.setText("Issue request to: ");
        issueRequestTo.setLayoutX(10);
        issueRequestTo.setLayoutY(57);

        ComboBox<String> nodes = new ComboBox<>();
        nodes.setLayoutX(120);
        nodes.setLayoutY(50);

        for (AKLNode node: Cluster.GetNodeList()) {
            nodes.getItems().add(node.Id);
        }

        TextField requestInput = new TextField();
        requestInput.setLayoutX(120);
        requestInput.setLayoutY(15);

        Button write = new Button();
        Button read = new Button();
        read.setText("Read");
        read.setMinWidth(100);
        read.setLayoutX(383);
        read.setLayoutY(15);
        read.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            String response = controller.read(requestInput.getText(), nodes.getSelectionModel().getSelectedItem());
                            requestInput.setEditable(false);
                            nodes.setDisable(true);
                            content.setText(response);
                            read.setDisable(true);
                            write.setDisable(true);
                            content.setEditable(false);
                        } catch (ProdException e) {
                            alertError("Unable to read request", e.getMessage());
                        }
                    }
                }
        );

        Button save = new Button();
        write.setText("Write");
        write.setLayoutX(383);
        write.setLayoutY(50);
        write.setMinWidth(100);
        write.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            String response[] = controller.write(requestInput.getText(), nodes.getSelectionModel().getSelectedItem());
                            edditingFile = response[0];
                            content.setText(response[1]);

                            save.setDisable(false);
                            requestInput.setEditable(false);
                            nodes.setDisable(true);
                            content.setEditable(true);
                            read.setDisable(true);
                            write.setDisable(true);
                        } catch (ProdException e) {
                            alertError("Unable to write to file.", e.getMessage());
                        }
                    }
                }
        );

        save.setText("Save");
        save.setDisable(true);
        save.setMinWidth(100);
        save.setLayoutX(280);
        save.setLayoutY(150);
        save.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            controller.save(edditingFile, content.getText());
                        } catch (ProdException e) {
                            alertError("Unable to save file", e.getMessage());
                        }
                    }
                }
        );

        Button releaseLock = new Button();
        releaseLock.setText("Release Lock");
        releaseLock.setLayoutX(383);
        releaseLock.setLayoutY(150);
        releaseLock.setMinWidth(100);
        releaseLock.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            controller.releaseLock(requestInput.getText(), nodes.getSelectionModel().getSelectedItem());

                            requestInput.setEditable(false);
                            nodes.setDisable(false);
                            read.setDisable(false);
                            write.setDisable(false);
                            content.setEditable(false);
                        } catch (ProdException e) {
                            alertError("Unable to release lock.", e.getMessage());
                        }
                    }
                }
        );

        ScrollPane pane = new ScrollPane();
        pane.setLayoutX(10);
        pane.setLayoutY(190);
        pane.setStyle("-fx-background-color:transparent;");
        pane.setMinWidth(WIDTH - 23);
        pane.setMaxWidth(WIDTH - 23);
        pane.setMinHeight(HEIGHT - 230);
        pane.setContent(content);
        pane.setPrefViewportHeight(HEIGHT - 300);

        root.getChildren().add(requestLabel);
        root.getChildren().add(pane);
        root.getChildren().add(write);
        root.getChildren().add(read);
        root.getChildren().add(save);
        root.getChildren().add(releaseLock);
        root.getChildren().add(requestInput);
        root.getChildren().add(issueRequestTo);
        root.getChildren().add(nodes);

        return clientWindow;
    }

    private void alertError(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.show();
    }
}
