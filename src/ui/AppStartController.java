package ui;

import akl.backend.*;
import akl.common.ProdException;
import javafx.stage.Stage;

import java.io.File;
import java.net.Socket;

public class AppStartController {
    public void spawnClient() throws ProdException {
        if (Cluster.GetNodeList().isEmpty()) {
            throw ProdException.NEED_TO_LAUNCH_BEFORE_SPAWN;
        }
        ClientScene clientScene = new ClientScene(java.util.UUID.randomUUID().toString());
        clientScene.launch().show();
    }

    public void launchCluster(String directory) throws ProdException {
        if (! new File(directory).isDirectory()) {
            throw ProdException.PATH_MUST_BE_DIR;
        }

        Cluster.LaunchCluster(directory);
    }

    public void shutdown() throws ProdException {
        Cluster.Shutdown();
    }
}
