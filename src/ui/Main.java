package ui;

import akl.backend.*;
import akl.common.ProdException;
import akl.common.Settings;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.Socket;

public class Main extends Application {
    private Stage primaryStage;
    AppStartScene launchScene;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        Settings.Instance.load();
        launchScene = new AppStartScene(this);
        launchScene.show();
    }

    @Override
    public void stop() {
        try {
            if (launchScene != null) {
                launchScene.close();
            }
            Cluster.Shutdown();
        } catch (ProdException whatever) {}
    }

    public Stage getStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
