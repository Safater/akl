package ui;

import akl.backend.*;
import akl.common.ProdException;

import java.io.*;
import java.net.Socket;

public class ClientController {
    private String guid;
    public ClientController(String guid) {
        this.guid = guid;
    }

    private AKLNode getNode(String node) throws ProdException {
        if (node == null || node.equals("")) {
            throw ProdException.NODE_NOT_SELECTED;
        }

        return Cluster.GetNode(node);
    }

    public String read(String path, String node) throws ProdException {
        final String[] content = { "", "" };

        AKLNode nodeInst = getNode(node);
        Client client = new Client(guid);
        try {
            client.connect("127.0.0.1", nodeInst.Port);
            Message message = getMessage(path, node, AKLLockManager.LockMode.Read);
            getContent(content, client);
            client.send(message);
        } finally {
            client.disconnect();
        }

        return content[1];
    }

    public void releaseLock(String path, String node) throws ProdException {
        AKLNode nodeInst = getNode(node);
        Client client = new Client(guid);
        try {
            client.connect("127.0.0.1", nodeInst.Port);
            MessageActionDefinition actionDefinition = new MessageActionDefinition();
            actionDefinition.LockMode = AKLLockManager.LockMode.None;
            actionDefinition.Subject = path;
            actionDefinition.From = guid;
            actionDefinition.Action = MessageActionDefinition.EAction.ReleaseLock;

            Message message = new Message();
            message.To = node;
            message.JSON = actionDefinition.buildJSON();
            client.setCallback(new ICallback() {
                public void respondTo(String messageGuid, Socket client) {}

                public void callback(Message message) throws ProdException {
                    String response = message.JSON;
                }
            });
            client.send(message);
        } finally {
            client.disconnect();
        }
    }

    public void save(String fullPath, String text) throws ProdException {
        FileWriter writer = null;
        try {
            writer = new FileWriter(fullPath);
            for (String s: text.split("\n")) {
                if (! s.equals("")) {
                    writer.write(s);
                }

                writer.write(System.lineSeparator());
            }
            writer.flush();
        } catch (Exception e) {
            ProdException.ThrowAsAKLException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignored) {}
            }
        }
    }

    public String[] write(String path, String node) throws ProdException {
        final String[] content = { "", "" };

        AKLNode nodeInst = getNode(node);
        Client client = new Client(guid);
        try {
            client.connect("127.0.0.1", nodeInst.Port);
            Message message = getMessage(path, node, AKLLockManager.LockMode.Write);
            getContent(content, client);
            client.send(message);
        } finally {
            client.disconnect();
        }

        return content;
    }

    private void getContent(String[] content, Client client) throws ProdException {
        client.setCallback(new ICallback() {
            public void respondTo(String messageGuid, Socket client) {}
            public void callback(Message message) throws ProdException {
                ResponseMessageDefinition response = new ResponseMessageDefinition();
                response.loadFromJSON(message.JSON);

                if (message.Code == Message.SUCCESS_CODE) {
                    try {
                        FileReader reader = new FileReader(response.FilePath);
                        StringBuilder contentBuffer = new StringBuilder();
                        char[] buffer = new char[1024];
                        while (reader.read(buffer, 0, 1024) > 0) {
                            contentBuffer.append(buffer);
                        }
                        content[0] = response.FilePath;
                        content[1] = contentBuffer.toString();
                    } catch (Exception e) {
                        ProdException.ThrowAsAKLException(e);
                    }
                } else {
                    throw ProdException.ACCESS_BLOCKED_TO_RESOURCE;
                }
            }
        });
    }

    private Message getMessage(String path, String node, AKLLockManager.LockMode lockMode) {
        MessageActionDefinition actionDefinition = new MessageActionDefinition();
        actionDefinition.LockMode = lockMode;
        actionDefinition.Subject = path;
        actionDefinition.From = guid;
        actionDefinition.Action = MessageActionDefinition.EAction.RequestAccess;

        Message message = new Message();
        message.To = node;
        message.JSON = actionDefinition.buildJSON();
        return message;
    }
}
