package ui;

import akl.backend.AKLNode;
import akl.backend.Cluster;
import akl.common.Logger;
import akl.common.ProdException;
import akl.common.Settings;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;


public class AppStartScene extends Scene {
    public static final String TITLE = "Distributed Lock Management System Sim.";
    public static final int WIDTH = 600;
    public static final int HEIGHT = 700;
    public static final int FONT_SIZE = 12;

    private Scene scene;
    private Application application;

    private AppStartController controller;
    private String chosenDirectory = Settings.Instance.InitialClusterLocation;

    private TextArea log;
    private Timer logUpdater;

    public AppStartScene(Application application) {
        super(new Group(), WIDTH, HEIGHT);
        this.application = application;

        ((Main) this.application).getStage().setTitle(TITLE);
        ((Main) this.application).getStage().setResizable(false);
        initializeScene();
        controller = new AppStartController();
    }

    private void initializeScene() {
        Group panel = (Group) this.getRoot();

        Label chosenDirectoryLabel = new Label();
        chosenDirectoryLabel.setText(chosenDirectory);
        chosenDirectoryLabel.setLayoutX(172);
        chosenDirectoryLabel.setLayoutY(15);
        chosenDirectoryLabel.setFont(Font.font(FONT_SIZE));

        Label directoryLabel = new Label();
        directoryLabel.setText("Chose cluster directory:");
        directoryLabel.setLayoutX(10);
        directoryLabel.setLayoutY(15);
        directoryLabel.setFont(Font.font(FONT_SIZE));

        Button directory = new Button();
        directory.setLayoutX(145);
        directory.setLayoutY(12);
        directory.setFont(Font.font(14));
        directory.setPadding(new Insets(0, 5, 0, 5));
        directory.setText("...");
        directory.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                DirectoryChooser chooser = new DirectoryChooser();
                File f = chooser.showDialog(((Main) application).getStage());
                if (f != null) {
                    chosenDirectory = f.getAbsolutePath();
                }
                chosenDirectoryLabel.setText(chosenDirectory);
                Settings.Instance.InitialClusterLocation = chosenDirectory;
                try {
                    Settings.Instance.save();
                } catch (ProdException ignored) {}
            }
        });

        Button stopCluster = new Button();
        Button refreshStatus = new Button();


        Label runningNodesLabel = new Label();
        runningNodesLabel.setText("Nodes in this cluster:");
        runningNodesLabel.setLayoutX(10);
        runningNodesLabel.setLayoutY(120);
        runningNodesLabel.setFont(Font.font(FONT_SIZE));

        ScrollPane runningNodes = new ScrollPane();
        Group nodesStack = new Group();
        runningNodes.setContent(nodesStack);
        runningNodes.setLayoutX(10);
        runningNodes.setLayoutY(150);
        runningNodes.setMinWidth(WIDTH - 11);
        runningNodes.setPrefViewportHeight(160);

        Button launchClusterButton = new Button();
        launchClusterButton.setText("Launch Cluster");
        launchClusterButton.setLayoutX(490);
        launchClusterButton.setLayoutY(10);
        launchClusterButton.setMinWidth(100);
        launchClusterButton.setFont(Font.font(FONT_SIZE));
        launchClusterButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    controller.launchCluster(chosenDirectory);
                    stopCluster.setDisable(false);
                    refreshStatus.setDisable(false);
                    initNodes(nodesStack);
                } catch (ProdException e) {
                    alertError("Unable to launch cluster", e.getMessage());
                }
            }
        });

        stopCluster.setText("Stop Cluster");
        stopCluster.setLayoutX(490);
        stopCluster.setLayoutY(360);
        stopCluster.setMinWidth(100);
        stopCluster.setDisable(true);
        stopCluster.setFont(Font.font(FONT_SIZE));
        stopCluster.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    controller.shutdown();
                    stopCluster.setDisable(true);
                    refreshStatus.setDisable(true);
                } catch (ProdException e) {
                    alertError("Unable to shutdown cluster", e.getMessage());
                }
            }
        });

        refreshStatus.setText("Refresh");
        refreshStatus.setLayoutX(375);
        refreshStatus.setLayoutY(360);
        refreshStatus.setMinWidth(100);
        refreshStatus.setDisable(true);
        refreshStatus.setFont(Font.font(FONT_SIZE));
        refreshStatus.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                initNodes(nodesStack);
            }
        });

        Button spawnClient = new Button();
        spawnClient.setText("Spawn Client");
        spawnClient.setLayoutX(490);
        spawnClient.setLayoutY(50);
        spawnClient.setMinWidth(100);
        spawnClient.setFont(Font.font(FONT_SIZE));
        spawnClient.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    controller.spawnClient();
                } catch (ProdException e) {
                    alertError("Unable to spawn client", e.getMessage());
                }
            }
        });


        log = new TextArea();
        log.setLayoutX(10);
        log.setLayoutY(400);
        log.setMinWidth(WIDTH - 11);
        log.setMinHeight(HEIGHT - 400);
        log.setEditable(false);

        logUpdater = new Timer();
        logUpdater.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                public void run() {
                    log.setText(Logger.Content());
                    log.positionCaret(Logger.Content().length());
                    log.setScrollTop(Logger.Content().length());
                }
            });
            }
        }, 0, 1000);



        panel.getChildren().add(chosenDirectoryLabel);
        panel.getChildren().add(runningNodesLabel);
        panel.getChildren().add(launchClusterButton);
        panel.getChildren().add(spawnClient);
        panel.getChildren().add(directoryLabel);
        panel.getChildren().add(runningNodes);
        panel.getChildren().add(stopCluster);
        panel.getChildren().add(refreshStatus);
        panel.getChildren().add(directory);
        panel.getChildren().add(log);
    }

    private void alertError(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.show();
    }

    private void initNodes(Group runningNodes) {
        runningNodes.getChildren().clear();

        double idX      = 10.0;
        double portX    = 270.0;
        double statusX  = 520.0;
        double y = 35.0;

        Label titlesId = new Label();
        titlesId.setText("ID");
        titlesId.setLayoutX(idX);
        titlesId.setLayoutY(5.0);
        titlesId.setFont(Font.font(14));
        runningNodes.getChildren().add(titlesId);

        Label titlesPort = new Label();
        titlesPort.setText("PORT");
        titlesPort.setLayoutX(portX);
        titlesPort.setLayoutY(5.0);
        titlesPort.setFont(Font.font(14));
        runningNodes.getChildren().add(titlesPort);

        Label titlesStatus = new Label();
        titlesStatus.setText("STATUS");
        titlesStatus.setLayoutX(statusX);
        titlesStatus.setLayoutY(5.0);
        titlesStatus.setFont(Font.font(14));
        runningNodes.getChildren().add(titlesStatus);

        for (AKLNode node: Cluster.GetNodeList()) {
            Label idLabel = new Label();
            idLabel.setText(node.Id);
            idLabel.setLayoutX(idX);
            idLabel.setLayoutY(y);
            idLabel.setFont(Font.font(14));
            runningNodes.getChildren().add(idLabel);

            Label portLabel = new Label();
            portLabel.setText(node.Port + "");
            portLabel.setLayoutX(portX);
            portLabel.setLayoutY(y);
            portLabel.setFont(Font.font(14));
            runningNodes.getChildren().add(portLabel);


            String status = "Idle";
            switch (node.Status) {
                case Running:
                    status = "Running";
                    break;
                case Suspended:
                    status = "Suspended";
                    break;
                case Idle:
                    break;
            }


            Label statusLabel = new Label();
            statusLabel.setText(status);
            statusLabel.setLayoutX(statusX);
            statusLabel.setLayoutY(y);
            statusLabel.setFont(Font.font(14));
            runningNodes.getChildren().add(statusLabel);
            y += 20;
        }
    }

    public void show() {
        ((Main) this.application).getStage().setScene(this);
        ((Main) this.application).getStage().show();
    }

    public void close() {
        if (logUpdater != null) {
            logUpdater.cancel();
            logUpdater.purge();
        }
    }
}
